# README.MD

Docker env. for developmnt.

* Build env. : 
```docker-compose up --build -d```

# Container : WP
* Use ```ln -s``` for link dev plugins on dev wordpress env.
* see README/*.png for configuration exemple

# Container : SimpleSAMLphp

* container : https://github.com/kristophjunge/docker-test-saml-idp

* For add new IDP : vi metadata/saml20-sp-remote.php
```php
<?php                                                                                         
/**                                                                                           
 * SAML 2.0 remote SP metadata for SimpleSAMLphp.                                             
 *                                                                                            
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote               
 */                                                                                           
                                                                                              
$metadata[getenv('SIMPLESAMLPHP_SP_ENTITY_ID')] = array(                                      
    'AssertionConsumerService' => getenv('SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE'),      
    'SingleLogoutService' => getenv('SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE'),                
);                                                                                            
                                                                                              
$metadata['http://localhost:82'] = array(                                                     
    'AssertionConsumerService' => 'http://localhost:82/wp-login.php',                         
    'SingleLogoutService' => 'http://localhost:82/wp-login.php?action=logout',                
);                                                                                            
                                                                                              
```

# Tips
* Generate certificate:
```openssl req -new -x509 -nodes -sha256 -out idp.crt -keyout idp.pem```
